﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JoaoSite.Models
{
    public class Estoque
    {
        public int Id { get; set; }
        public int ModeloId { get; set; }
        public int Quantidade { get; set; }
    }
}
