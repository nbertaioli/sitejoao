﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JoaoSite.Models
{
    public class Anuncio
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Modelo do veículo é obrigatório")]
        public int ModeloId { get; set; }

        [Required(ErrorMessage = "Valor de compra é obrigatório")]
        public decimal ValorCompra { get; set; }

        [Required(ErrorMessage = "Valor de venda é obrigatório")]
        public decimal ValorVenda { get; set; }

        [Required(ErrorMessage = "Cor é um campo obrigatório")]
        public string Cor { get; set; }

        [Required(ErrorMessage = "Combustível é um campo obrigatório")]
        public string Combustivel { get; set; }

        [Required(ErrorMessage = "Data da venda é obrigatório")]
        public DateTime DataVenda { get; set; }
    }
}
