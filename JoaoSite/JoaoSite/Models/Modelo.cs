﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JoaoSite.Models
{
    public class Modelo
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public int AnoLancamento { get; set; }
        public int MarcaId { get; set; }
        public Estoque Quantidade { get; set; }
    }
}
