﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JoaoSite.Models
{
    public class Marca
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public List<Modelo> Modelos { get; set; }
    }
}
