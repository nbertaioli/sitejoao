﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JoaoSite.Context;
using JoaoSite.Models;
using Microsoft.AspNetCore.Mvc;
using PagedList;
using Rotativa;
using Rotativa.AspNetCore;

namespace JoaoSite.Controllers
{
    public class RelatorioController : Controller
    {
        private readonly AppDbContext _context;

        public RelatorioController(AppDbContext contexto)
        {
            _context = contexto;
        }

        [HttpPost]
        public ViewAsPdf RelatorioPedidos(string dataInicial, string dataFinal)
        {
            int pagNumero = 1;
            var relatorioPDF = new ViewAsPdf();
            relatorioPDF.ViewName = "RelatorioPedidos";
            relatorioPDF.FileName = "RelatorioPedidos.pdf";
            relatorioPDF.IsGrayScale = true;

            var listaAnuncios = _context.Anuncios.Where(a => a.DataVenda >= Convert.ToDateTime(dataInicial) && a.DataVenda <= Convert.ToDateTime(dataFinal)).OrderBy(c => c.ModeloId).ToList();

            if (listaAnuncios.Count() <= 0)
            {
                Anuncio anuncio = new Anuncio();
                anuncio.ModeloId = 999;
                anuncio.ValorVenda = 0;
                anuncio.ValorCompra = 0;
                listaAnuncios.Add(anuncio);
            }
            
            relatorioPDF.Model = listaAnuncios.ToPagedList(pagNumero, listaAnuncios.Count);
            return relatorioPDF;

        }
    }
}