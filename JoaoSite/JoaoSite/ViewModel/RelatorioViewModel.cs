﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JoaoSite.ViewModel
{
    public class RelatorioViewModel
    {
        public int ModeloId { get; set; }
        public DateTime DataVenda { get; set; }
        public decimal ValorTotal { get; set; }
    }
}
