﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using API_SiteJoao.App_Start;
using API_SiteJoao.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_SiteJoao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnuncioController : ControllerBase
    {
        private static List<Anuncio> modelos = new List<Anuncio>();

        SqlCommand sqlCom = new SqlCommand();

        [HttpGet]
        public List<Anuncio> GetListClient(int id)
        {
            try
            {
                modelos.Clear();

                Connection con = new Connection();
                sqlCom.Connection = con.Connect();

                sqlCom.CommandText = "SELECT ID, MODELOID, VALORCOMPRA, VALORVENDA, COR, COMBUSTIVEL, DATAVENDA FROM ANUNCIOS WHERE ID = @id";
                sqlCom.Parameters.AddWithValue("@id", id);
                var result = sqlCom.ExecuteReader();

                while (result.Read())
                {
                    Anuncio tempAnuncio = new Anuncio(
                        Convert.ToInt32(result["ID"].ToString()),
                        Convert.ToInt32(result["MODELOID"].ToString()),
                        Convert.ToDecimal(result["VALORCOMPRA"].ToString()),
                        Convert.ToDecimal(result["VALORVENDA"].ToString()),
                        result["COR"].ToString(),
                        result["COMBUSTIVEL"].ToString(),
                        Convert.ToDateTime(result["DataVenda"].ToString())
                    );
                    modelos.Add(tempAnuncio);
                }

                con.Disconnect();

                return modelos;
            }
            catch (Exception e)
            {
                Console.WriteLine("Ocorreu um erro ao processar a pesquisa." + e.Message);
            }

            return modelos;
        }
    }
}