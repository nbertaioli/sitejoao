﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_SiteJoao.Models
{
    public class Anuncio
    {
        public int Id { get; set; }
        public int ModeloId { get; set; }
        public decimal ValorCompra { get; set; }
        public decimal ValorVenda { get; set; }
        public string Cor { get; set; }
        public string Combustivel { get; set; }
        public DateTime DataVenda { get; set; }

        public Anuncio(int id, int modeloId, decimal valorCompra, decimal valorVenda, string cor, string combustivel, DateTime dataVenda)
        {
            this.Id = id;
            this.ModeloId = modeloId;
            this.ValorCompra = valorCompra;
            this.ValorVenda = valorVenda;
            this.Cor = cor;
            this.Combustivel = combustivel;
            this.DataVenda = dataVenda;

        }
    }
}
